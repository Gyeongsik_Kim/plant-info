package com.study.info;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private String ip;
    private int port;

    private void setupServer(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("서버 설정");
        final LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        final EditText eip = new EditText(this);
        eip.setInputType(InputType.TYPE_CLASS_PHONE);
        eip.setHint("IP");

        final EditText eport = new EditText(this);
        eport.setInputType(InputType.TYPE_CLASS_NUMBER);
        eport.setHint("PORT");

        linearLayout.addView(eip);
        linearLayout.addView(eport);

        builder.setView(linearLayout);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ip = eip.getText().toString();
                port = Integer.valueOf(eport.getText().toString());
                Toast.makeText(MainActivity.this, "IP " + ip, Toast.LENGTH_SHORT).show();
                Toast.makeText(MainActivity.this, "PORT " + port, Toast.LENGTH_SHORT).show();
                SharedPreferences preferences = getSharedPreferences("info", MODE_PRIVATE);
                int type = preferences.getInt("type", 0);
                if(type != 0){
                    Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                    intent.putExtra("type", type);
                    intent.putExtra("ip", ip);
                    intent.putExtra("port", port);
                    startActivity(intent);
                    finish();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // If a notification message is tapped, any data accompanying the notification
        // message is available in the intent extras. In this sample the launcher
        // intent is fired when the notification is tapped, so any accompanying data would
        // be handled here. If you want a different intent fired, set the click_action
        // field of the notification message to the desired intent. The launcher intent
        // is used when no click_action is specified.
        //
        // Handle possible data accompanying notification message.
        // [START handle_data_extras]

        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }
        // [END handle_data_extras]

        FirebaseMessaging.getInstance().subscribeToTopic("news");
        // [END subscribe_topics]

        // Log and toast
        //String msg = getString(R.string.msg_subscribed);
        //Log.d(TAG, msg);
        //Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();

        // Get token
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.i("TOKEN", token);
        // Log and toast
        //String msg = getString(R.string.msg_token_fmt, token);
        //Log.d(TAG, msg);
        //Toast.makeText(MainActivity.this, token, Toast.LENGTH_SHORT).show();
        setupServer();
    }

    public void first(View view){
        setType(1);

    }

    public void second(View view){
        setType(2);
    }

    public void third(View view){
        setType(3);
    }

    private void setType(int value){
        SharedPreferences preferences = getSharedPreferences("info", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("type", value);
        editor.apply();
        Intent intent = new Intent(MainActivity.this, InfoActivity.class);
        intent.putExtra("type", value);
        intent.putExtra("ip", ip);
        intent.putExtra("port", port);
        startActivity(intent);
        finish();
    }
}
